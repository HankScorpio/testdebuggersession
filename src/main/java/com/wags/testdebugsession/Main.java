package com.wags.testdebugsession;

/**
 * Test Possible bug in Netbeans, IntelliJ IDEA 2016 and Eclipse Try to debug
 * Debug this app, step by step (use step over)
 *
 * @author Wilmar Giraldo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int count = 0; // Add "count" variable to watch list
        int iteration = 0; //Add "iteration" variable to watch list
        System.out.println("Count (before while loop): " + count); // Set a breakpoint in this line
        while (count++ < 3) { // Set (count++ < 3) to watch list
            iteration++;
            System.out.println("Count (into loop): " + count);
            System.out.println("Iteration:         " + iteration);
        }
        System.out.println("Count (after while loop): " + count);
    }

}

/*
* Comments:
* In Netbeans, set enabled "Show watches inside Variables view" option for see the bug.
* In Eclipse, is the default behaviour
* In IntelliJ IDEA, is the default behaviour
* Oracle JDeveloper run debug session correctly
*/
